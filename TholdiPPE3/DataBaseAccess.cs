﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace TholdiPPE3
{
    static class DataBaseAccess
    {
        private static string connectionString = "Database=gest-container;Data Source=localhost;User Id=root;Password=";
        
        

        public static MySqlConnection GetConnexion
        {
            get
            {
                return new MySqlConnection(connectionString);
            }
        }

       

        public static MySqlParameter CodeParam(string paramName, object value)
        {
            MySqlCommand commandSql = new MySqlCommand();
            MySqlParameter parametre = commandSql.CreateParameter();
            parametre.ParameterName = paramName;
            parametre.Value = value;
            return parametre;
        }

        
    }
}

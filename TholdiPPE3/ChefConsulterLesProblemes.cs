﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace TholdiPPE3
{
    public partial class ChefConsulterLesProblemes : Form
    {
        public ChefConsulterLesProblemes()
        {
            InitializeComponent();
        }

       

        private void ChefConsulterLesProblemes_Load(object sender, EventArgs e)
        {
            
            List<Declaration> collectionDeclaration = Declaration.FetchAll(0);
            dataGridView1.DataSource = collectionDeclaration;
            dataGridView1.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
            dataGridView1.Columns[0].HeaderText = "Nom du problème";
            dataGridView1.Columns[1].HeaderText = "Code de la déclaration";
            dataGridView1.Columns[2].HeaderText = "Date de la déclaration";
            dataGridView1.Columns[3].HeaderText = "Code du problème";
            dataGridView1.Columns[4].HeaderText = "Commentaire";
            dataGridView1.Columns[5].HeaderText = "Urgenre";
            dataGridView1.Columns[6].HeaderText = "Traite";
            dataGridView1.Columns[7].HeaderText = "Numéro container";




        }

        private void button2_Click(object sender, EventArgs e)
        {
            Thread t = new Thread(() => Application.Run(new IhmChef()));
            t.Start();
            this.Close();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace TholdiPPE3
{
    class Containers
    {
        private int numContainer;
        private DateTime dateAchat;
        private string typeContainer;
        private DateTime dateLimiteProchInsp;

        public string getTypeContainer
        {
            get { return typeContainer; }
        }
        public int GetnumContainer
        {
            get { return numContainer; }
        }

        public DateTime DateLimiteProchInsp
        {
            get
            {
                return dateLimiteProchInsp;
            }

            set
            {
                dateLimiteProchInsp = value;
            }
        }

        #region Requête SQL

        private static string _selectSql =
            "SELECT numContainer, dateAchat  FROM CONTAINER";

        private static string _selectBynumContainerSql =
            "SELECT numContainer, dateAchat, typeContainer, dateLimiteProchInsp FROM CONTAINER WHERE numContainer = ?numContainer";

        private static string _updateSql =
            "UPDATE CONTAINER SET dateLimiteProchInsp = ?dateLimiteProchInsp  WHERE numContainer = ?numContainer";

        #endregion

        #region méthode d'accès aux données

        public static Containers Fetch(int numContainer)
        {
            Containers c = null;
            MySqlConnection msc = DataBaseAccess.GetConnexion;
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _selectBynumContainerSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?numContainer", numContainer));
            commandSql.Prepare();
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            bool existEnregistrement = jeuEnregistrements.Read();
            if (existEnregistrement)
            {
                c = new Containers();
                c.numContainer = Convert.ToInt32(jeuEnregistrements["numContainer"].ToString());
                c.dateAchat = Convert.ToDateTime(jeuEnregistrements["dateAchat"].ToString());
                c.typeContainer = jeuEnregistrements["typeContainer"].ToString();
                c.dateLimiteProchInsp = Convert.ToDateTime(jeuEnregistrements["dateLimiteProchInsp"].ToString());
            }
            msc.Close();
            return c;
        }

        public static List<Containers> FetchAll()
        {
            List<Containers> collectionContainer = new List<Containers>();
            MySqlConnection msc = DataBaseAccess.GetConnexion;
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _selectSql;
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            while (jeuEnregistrements.Read())
            {
                Containers c = new Containers();
                c.numContainer = Convert.ToInt16(jeuEnregistrements["numContainer"].ToString());
                c.dateAchat = Convert.ToDateTime(jeuEnregistrements["dateAchat"].ToString());
                
                collectionContainer.Add(c);
            }
            msc.Close();
            return collectionContainer;
        }

        #endregion


    }
}

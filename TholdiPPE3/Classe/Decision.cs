﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace TholdiPPE3
{
    class Decision
    {
        private int numInspection;
        private string codeTravaux;
        private DateTime dateEnvoi;
        private DateTime dateRetour;
        private string commentaire;
        
        private static string _selectSql =
         "SELECT codeTravaux ,  numInspection , dateEnvoi , dateRetour , commentaire FROM Decision";

        private static string _selectBynumInspectionSql =
            "SELECT codeTravaux ,  numInspection , dateEnvoi , dateRetour , commentaire  FROM Decision WHERE numInspection = ?numInspection ";

        private static string _updateSql =
            "UPDATE Decision SET numInspection=?numInspection  WHERE numInspection=?numInspection ";

        private static string _insertSql =
            "INSERT INTO Decision VALUES (?dateEnvoi , ?dateRetour , ?commentaire , ?numInspection , ?codeTravaux)";

        private static string _deleteBynumInspectionSql =
            "DELETE FROM Decision WHERE numInspection = ?numInspection";

        private static string _getLastInsertnumInspection =
            "SELECT numInspection FROM Decision WHERE numInspection=?numInspection";

        public static Decision Fetch (int numInspection)
        {
            Decision c = null;
            MySqlConnection msc = DataBaseAccess.GetConnexion;
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _selectBynumInspectionSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?numInspection", numInspection));
            commandSql.Prepare();
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            bool existEnregistrement = jeuEnregistrements.Read();
            if (existEnregistrement)
            {
                c = new Decision();
                c.codeTravaux = jeuEnregistrements["codeTravaux"].ToString();
                c.numInspection = Convert.ToInt16(jeuEnregistrements["numInspection"].ToString());
                c.dateEnvoi = Convert.ToDateTime(jeuEnregistrements["dateEnvoi"].ToString());
                c.dateRetour = Convert.ToDateTime(jeuEnregistrements["dateRetour"].ToString());
                c.commentaire = jeuEnregistrements["commentaire"].ToString();

            }
            msc.Close();
            return c;
        }
        public void Update()
        {
            MySqlConnection msc = DataBaseAccess.GetConnexion;
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _updateSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?numInspection", numInspection));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?codeTravaux", codeTravaux));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?dateEnvoi", dateEnvoi));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?dateRetour", dateRetour));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?commentaire", commentaire));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();
            numInspection = -1;
            msc.Close();
        }

        public void Insert()
        {
            MySqlConnection msc = DataBaseAccess.GetConnexion;
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _insertSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?numInspection", numInspection));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();
            msc.Close();
        }
        public void Save()
        {
            if(numInspection == -1)
            {
                Insert();
            }
            else
            {
                Update();
            }
        }
       
        }
    }
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace TholdiPPE3
{
    class Probleme
    {
        private int codeProbleme;
        private string libelleProbleme;

        #region Requête SQL
        public int GetcodeProbleme
        {
            get { return codeProbleme; }
        }
        public string GetlibelleProbleme
        {
            get { return libelleProbleme; }
        }
        private static string _selectSql =
            "SELECT codeProbleme,libelleProbleme FROM PROBLEME";

        private static string _selectBycodeProblemeSql =
            "SELECT codeProbleme,libelleProbleme FROM PROBLEME WHERE codeProbleme = ?codeProbleme";

        private static string _selectLibelleSql =
            "Select libelleProbleme from Probleme";
        #endregion

       
        #region méthode d'accès aux données
        public static int LaComparaison(string libelleProbleme)
        {
            int codeProbleme = 0;

            if (libelleProbleme == "Corrosion")
            {
                codeProbleme = 2;
            }
            if (libelleProbleme == "Choc sur container")
            {
                codeProbleme = 3;
            }
            if (libelleProbleme == "Gong defectueux")
            {
                codeProbleme = 4;
            }
            if (libelleProbleme == "Problème refrigeration")
            {
                codeProbleme = 5;
            }

            return codeProbleme;
        }
        public static Probleme Fetch(int codeProbleme)
        {
            Probleme p = null;
            MySqlConnection msc = DataBaseAccess.GetConnexion;
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _selectBycodeProblemeSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?codeProbleme", codeProbleme));
            commandSql.Prepare();
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            bool existEnregistrement = jeuEnregistrements.Read();
            if (existEnregistrement)
            {
                p = new Probleme();
                p.codeProbleme = Convert.ToInt16(jeuEnregistrements["codeProbleme"].ToString());
                p.libelleProbleme = jeuEnregistrements["libelleProbleme"].ToString();
            }
            msc.Close();
            return p;
        }

        public static List<Probleme> FetchAll()
        {
            List<Probleme> collectionProbleme = new List<Probleme>();
            MySqlConnection msc = DataBaseAccess.GetConnexion;
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _selectSql;
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            while (jeuEnregistrements.Read())
            {
                Probleme p = new Probleme();
                p.codeProbleme = Convert.ToInt16(jeuEnregistrements["codeProbleme"].ToString());
                p.libelleProbleme = jeuEnregistrements["libelleProbleme"].ToString();
                collectionProbleme.Add(p);
            }
            msc.Close();
            return collectionProbleme;
        }

        #endregion
    }
}

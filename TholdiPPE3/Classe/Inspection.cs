﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace TholdiPPE3
{
    class Inspection
    {
        private int numInspection;
        private DateTime dateInspection;
        private string commentairePostInspection;
        private string avis;
        private string motif;
        private string etat;
        private int NumContainer;

        #region Accesseurs
        public Inspection(DateTime dateInspection , string uncommentaire , string unavis , string unmotif , string unetat, int numContainer)
            {
            
            this.dateInspection = dateInspection;
            this.commentairePostInspection = uncommentaire;
            this.avis = unavis;
            this.motif = unmotif;
            this.etat = unetat;
            this.NumContainer = numContainer;
            }

        public Inspection()
        {

        }

        public int GetnumInspection
        {
            get { return numInspection; }
        }
        public string getetat
        {
            get { return etat; }
            set { etat = value; }
        }
        public string getcommentairePostInspection
        {
            get { return commentairePostInspection; }
        }
        public string getavis
        {
            get { return avis; }
        }
        public string getmotif
        {
            get { return motif; }
        }
        public int getNumContainer
        {
            get { return NumContainer; }
        }
        #endregion

        private static string  _selectSql =
             "SELECT numInspection ,  NumContainer , dateInspection , commentairePostInspection , avis , motif , etat  FROM INSPECTION";

          private static string _updateSql =
            "UPDATE INSPECTION SET numInspection=?nomInspection  WHERE nomInspection=?nomInspection ";

        private static string _insertSql =
            "INSERT INTO INSPECTION  VALUES (?numInspection,?dateInspection,?commentairePostInspection,?avis,?etat,?motif,?numContainer)";
        private static string _selectBynumInspectionSql =
            "SELECT numInspection , dateInspection , commentairePostInspection , avis , motif , etat , NumContainer FROM Inspection WHERE numInspection=?numInspection";
        private static string _selectBynumContainerSql =
         "SELECT numInspection , dateInspection , commentairePostInspection , avis , motif , etat , NumContainer FROM Inspection WHERE NumContainer=?NumContainer";
        private static string _deleteBynumInspectionSql =
            "DELETE FROM Inspection WHERE numInspection = ?numInspection";
        private static string _selectByetatSql =
            "SELECT numInspection, NumContainer, dateInspection, commentairePostInspection, avis, motif, etat  FROM INSPECTION WHERE etat=?etat";
        private static string _selectByMotifSql =
            "Select motif FROM INSPECTION";
        private static string _selectEnum =
            "SHOW COLUMNS FROM INSPECTION LIKE 'motif'";

        public static List<String> FetchAllMotif()
        {
            List<String> uneListe = new List<String>();
            MySqlConnection msc = DataBaseAccess.GetConnexion;
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _selectEnum;
            
            commandSql.Prepare();
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            bool existEnregistrement = jeuEnregistrements.Read();
            if (existEnregistrement)
            {
               
                string motif = jeuEnregistrements.ToString();
                motif.Replace("enum('", "");
                motif.Replace("')", "");
                motif.Replace("','", "");
              
               
                uneListe.Add(motif);
            }
            msc.Close();
            return uneListe;
        }

        public static Inspection Fetch(string etat)
        {
            Inspection c = null;
            MySqlConnection msc = DataBaseAccess.GetConnexion;
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _selectByetatSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?etat", etat));
            commandSql.Prepare();
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            bool existEnregistrement = jeuEnregistrements.Read();
            if (existEnregistrement)
            {
                c = new Inspection();
                c.numInspection = Convert.ToInt16(jeuEnregistrements["numInspection"].ToString());
                c.dateInspection = Convert.ToDateTime(jeuEnregistrements["dateInspection"].ToString());
                c.commentairePostInspection = jeuEnregistrements["commentairePostInspection"].ToString();
                c.avis = jeuEnregistrements["avis"].ToString();
                c.motif = jeuEnregistrements["motif"].ToString();
                c.etat = jeuEnregistrements["etat"].ToString();
                c.NumContainer = Convert.ToInt16(jeuEnregistrements["NumContainer"].ToString());
            }
            msc.Close();
            return c;
        }
        
        public void Save()
        {
            if(numInspection == -1)
            {
                AjouterInspection();
            }
            else
            {
                Update();
            }
        }
        public void AjouterInspection ()
        {
            MySqlConnection msc = DataBaseAccess.GetConnexion;            
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _insertSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?numInspection", numInspection));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?dateInspection", dateInspection));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?commentairePostInspection", commentairePostInspection));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?avis", avis));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?etat", etat));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?motif", motif));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?numContainer", NumContainer));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();
            msc.Close();

        }
        private void Delete()
        {
            MySqlConnection msc = DataBaseAccess.GetConnexion;
            msc.Open();
            MySqlCommand commandSql = DataBaseAccess.GetConnexion.CreateCommand();
            commandSql.CommandText = _deleteBynumInspectionSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?numInspection", numInspection));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();
            numInspection = -1;
            msc.Close();
        }
        private void Update()
        {
            MySqlConnection msc = DataBaseAccess.GetConnexion;           
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _updateSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?numInspection", numInspection));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?NumContainer", NumContainer));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();
            msc.Close();
        }
        public static List<Inspection> FetchAll(int NumContainer)
        {
            List<Inspection> resultat = new List<Inspection>();
            MySqlConnection msc = DataBaseAccess.GetConnexion;
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _selectBynumContainerSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?NumContainer", NumContainer));
            commandSql.Prepare();
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            while (jeuEnregistrements.Read())
            {
                Inspection inspection = new Inspection();
                string numInspectionInspection = jeuEnregistrements["numInspection"].ToString();
                string dateInspectionInspection = jeuEnregistrements["dateInspection"].ToString();
                string avisInspectionInspection = jeuEnregistrements["avis"].ToString();
                string etatInspectionInspection = jeuEnregistrements["etat"].ToString();
                string motifInspectionInspection = jeuEnregistrements["motif"].ToString();
                string commentaireInspectionInspection = jeuEnregistrements["commentairePostInspection"].ToString();
                string numContainerInspection = jeuEnregistrements["NumContainer"].ToString();
                inspection.numInspection = Convert.ToInt16(numInspectionInspection);
                inspection.dateInspection = Convert.ToDateTime(dateInspectionInspection);
                inspection.avis = avisInspectionInspection;
                inspection.motif = motifInspectionInspection;
                inspection.commentairePostInspection = commentaireInspectionInspection;
                inspection.etat = etatInspectionInspection;
                inspection.NumContainer = Convert.ToInt16(numContainerInspection);
                resultat.Add(inspection);
            }
            msc.Close();
            return resultat;
        }
        public static List<Inspection> FetchAll()
        {
            List<Inspection> resultat = new List<Inspection>();
            MySqlConnection msc = DataBaseAccess.GetConnexion;
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _selectSql;
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            while (jeuEnregistrements.Read())
            {
                Inspection inspection = new Inspection();
                string numInspectionInspection = jeuEnregistrements["numInspection"].ToString();
                string dateInspectionInspection = jeuEnregistrements["dateInspection"].ToString();
                string avisInspectionInspection = jeuEnregistrements["avis"].ToString();
                string etatInspectionInspection = jeuEnregistrements["etat"].ToString();
                string motifInspectionInspection = jeuEnregistrements["motif"].ToString();
                string commentaireInspectionInspection = jeuEnregistrements["commentairePostInspection"].ToString();
                string numContainerInspection = jeuEnregistrements["NumContainer"].ToString();
                inspection.numInspection = Convert.ToInt16(numInspectionInspection);
                inspection.dateInspection = Convert.ToDateTime(dateInspectionInspection);
                inspection.avis = avisInspectionInspection;
                inspection.motif = motifInspectionInspection;
                inspection.commentairePostInspection = commentaireInspectionInspection;
                inspection.etat = etatInspectionInspection;
                inspection.NumContainer = Convert.ToInt16(numContainerInspection);
                resultat.Add(inspection);
            }
            msc.Close();
            return resultat;
        }
        public static List<Inspection> FetchAll(string etat)
        {
            List<Inspection> resultat = new List<Inspection>();
            MySqlConnection msc = DataBaseAccess.GetConnexion;
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _selectByetatSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?etat", etat));
            commandSql.Prepare(); 
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            while (jeuEnregistrements.Read())
            {
                Inspection inspection = new Inspection();
                string numInspectionInspection = jeuEnregistrements["numInspection"].ToString();
                string dateInspectionInspection = jeuEnregistrements["dateInspection"].ToString();
                string avisInspectionInspection = jeuEnregistrements["avis"].ToString();
                string etatInspectionInspection = jeuEnregistrements["etat"].ToString();
                string motifInspectionInspection = jeuEnregistrements["motif"].ToString();
                string commentaireInspectionInspection = jeuEnregistrements["commentairePostInspection"].ToString();
                string numContainerInspection = jeuEnregistrements["NumContainer"].ToString();
                inspection.numInspection = Convert.ToInt16(numInspectionInspection);
                inspection.dateInspection = Convert.ToDateTime(dateInspectionInspection);
                inspection.avis = avisInspectionInspection;
                inspection.motif = motifInspectionInspection;
                inspection.commentairePostInspection = commentaireInspectionInspection;
                inspection.etat = etatInspectionInspection;
                inspection.NumContainer = Convert.ToInt16(numContainerInspection);
                resultat.Add(inspection);
            }
            msc.Close();
            return resultat;
        }
    }
}

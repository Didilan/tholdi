﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace TholdiPPE3
{
    class Declaration
    {
        #region Modele Objet
        private int codeDeclaration;
        private string commentaireDeclaration;
        private DateTime dateDeclaration;
        private int urgence;
        private int traite;
        private int codeProbleme;
        private int numContainer;
        private string libelleProbleme;

        public Declaration(string commentaireDeclaration , DateTime dateDeclaration , int urgence , int traite , int codeProbleme , int numContainer)
        {
            this.commentaireDeclaration = commentaireDeclaration;
            this.dateDeclaration = dateDeclaration;
            this.urgence = urgence;
            this.traite = traite;
            this.codeProbleme = codeProbleme;
            this.numContainer = numContainer;
        }

        public Declaration()
        {

        }
        public string getlibelleProbleme
        {
            get { return libelleProbleme; }
            set { libelleProbleme = value; }
        }
        public int getcodeDeclaration
        {
            get { return codeDeclaration; }
            set { codeDeclaration = value; }
        }
        public DateTime getdateDeclaration
        {
            get { return dateDeclaration; }
            set { dateDeclaration = value; }
        }
        private List<Declaration> _sesDeclarations;


        public int getcodeProbleme
        {
            get { return codeProbleme; }
            set { codeProbleme = value; }
        }

        public string getcommentaire
        {
            get { return commentaireDeclaration; }
            set { commentaireDeclaration = value; }
        }
        public int geturgence
        {
            get { return urgence; }
            set { urgence = value; }
        }
        public int gettraite
        {
            get { return traite; }
            set { traite = value; }
        }
        public int getNumContainer
        {
            get { return numContainer; }
            set { numContainer = value; }
        } // a transformer en Container container 





        #endregion

        #region Champs à portée classe contenant l'ensemble des requêtes d'accès aux données
        private static string _selectSql =
                "SELECT codeDeclaration,commentaireDeclaration,dateDeclaration,urgence,traite,codeProbleme,numContainer FROM Declaration";

        private static string _selectByCodeDeclarationSql =
            "SELECT codeDeclaration , dateDeclaration  FROM Declaration WHERE codeDeclaration = ?codeDeclaration ";

        private static string _updateSql =
            "UPDATE Declaration SET dateDeclaration=?dateDeclaration  WHERE codeDeclaration=?codeDeclaration ";

        private static string _insertSql =
            "INSERT INTO Declaration (dateDeclaration) VALUES (?dateDeclaration)";

        private static string _insertSqll =
             "INSERT INTO Declaration  VALUES (?codeDeclaration,?commentaireDeclaration,?dateDeclaration,?urgence,?traite,?codeProbleme,?numContainer)";

        private static string _deleteByCodeDeclarationSql =
            "DELETE FROM Declaration WHERE codeDeclaration = ?codeDeclaration";

        private static string _selectByTraiteSql =
            "SELECT codeDeclaration,commentaireDeclaration,dateDeclaration,urgence,traite,codeProbleme,numContainer   FROM Declaration WHERE traite = ?traite ";
        private static string _selectlibelle =
            "Select libelleProleme from Probleme";

        #endregion
       
        public static Declaration Fetch(string etat)
        {
            Declaration d = null;
            MySqlConnection msc = DataBaseAccess.GetConnexion;
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _selectByTraiteSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?etat", etat));
            commandSql.Prepare();
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            bool existEnregistrement = jeuEnregistrements.Read();
            if (existEnregistrement)
            {
                d = new Declaration();
                d.codeDeclaration = Convert.ToInt16(jeuEnregistrements["codeDeclaration"].ToString());
                d.numContainer = Convert.ToInt16(jeuEnregistrements["dateInspection"].ToString());
                d.codeProbleme = Convert.ToInt16(jeuEnregistrements["codeProbleme"].ToString());
                d.commentaireDeclaration = jeuEnregistrements["commentaireDeclaration"].ToString();
                d.dateDeclaration = Convert.ToDateTime(jeuEnregistrements["dateDeclaration"].ToString());
                d.urgence = Convert.ToInt16(jeuEnregistrements["urgence"].ToString());
                d.traite = Convert.ToInt16(jeuEnregistrements["traite"].ToString());
            }
            msc.Close();
            return d;
        }





        #region Méthodes d'accès aux données

        /// <summary>
        /// Valorise un objet Client depuis le système de gestion de bases de données
        /// </summary>
        /// <param name="numClient">La valeur de la clé primaire</param>
        public static Declaration Fetch(int codeDeclaration)
        {
            Declaration d = null;
            MySqlConnection msc = DataBaseAccess.GetConnexion;
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _selectByCodeDeclarationSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?codeDeclaration", codeDeclaration));
            commandSql.Prepare();
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            bool existEnregistrement = jeuEnregistrements.Read();
            if (existEnregistrement)
            {//alors
                d = new Declaration();
                d.codeDeclaration = Convert.ToInt16(jeuEnregistrements["codeDeclaration"].ToString());
                d.dateDeclaration = Convert.ToDateTime(jeuEnregistrements["nomClient"].ToString());
                //d._sesDeclarations = Declaration.Fetch(d);
            }
            msc.Close();//fermeture de la connexion
            return d;
        }

        /// <summary>
        /// Sauvegarde ou met à jour un Client dans la base de données
        /// </summary>
        public void Save()
        {
            if (codeDeclaration == -1)
            {
                Insert();
            }
            else
            {
                Update();
            }
            SaveDeclaration();
        }

        /// <summary>
        /// Supprime le Client représenté par l'instance courante dans le SGBD
        /// </summary>
        public void Delete()
        {
            MySqlConnection msc = DataBaseAccess.GetConnexion;
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _deleteByCodeDeclarationSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?codeDeclaration", codeDeclaration));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();
            codeDeclaration = -1;
            msc.Close();
        }

        /// <summary>
        /// Retourne une collection contenant les Clients
        /// </summary>
        /// <returns>Une collection de Clients</returns>
        public static List<Declaration> FetchAll()
        {
            List<Declaration> resultat = new List<Declaration>();
            MySqlConnection msc = DataBaseAccess.GetConnexion;
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _selectSql;
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            while (jeuEnregistrements.Read())
            {
                Declaration declaration = new Declaration();
                declaration.codeDeclaration = Convert.ToInt16(jeuEnregistrements["codeDeclaration"].ToString());
                declaration.commentaireDeclaration = jeuEnregistrements["commentaireDeclaration"].ToString();
                declaration.dateDeclaration = Convert.ToDateTime(jeuEnregistrements["dateDeclaration"].ToString());
                declaration.urgence = Convert.ToInt16(jeuEnregistrements["urgence"].ToString());
                declaration.traite = Convert.ToInt16(jeuEnregistrements["traite"].ToString());
                declaration.codeProbleme = Convert.ToInt16(jeuEnregistrements["codeProbleme"].ToString());
                if (Convert.ToInt16(jeuEnregistrements["codeProbleme"].ToString()) == 2)
                {
                    declaration.libelleProbleme = "Corrosion";
                }
                if (Convert.ToInt16(jeuEnregistrements["codeProbleme"].ToString()) == 3)
                {
                    declaration.libelleProbleme = "Choc sur container";
                }
                if (Convert.ToInt16(jeuEnregistrements["codeProbleme"].ToString()) == 4)
                {
                    declaration.libelleProbleme = "Gong defectueux";
                }
                if (Convert.ToInt16(jeuEnregistrements["codeProbleme"].ToString()) == 5)
                {
                    declaration.libelleProbleme = "Problème de refrigeration";
                }
                declaration.numContainer = Convert.ToInt16(jeuEnregistrements["numContainer"].ToString());
                resultat.Add(declaration);
            }
            msc.Close();
            return resultat;
        }

        private void Update()
        {
            MySqlConnection msc = DataBaseAccess.GetConnexion;
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _updateSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?codeDeclaration", codeDeclaration));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?dateDeclaration", dateDeclaration));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();
            msc.Close();
        }

        private void Insert()
        {
            MySqlConnection msc = DataBaseAccess.GetConnexion;
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _insertSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?dateDeclaration", dateDeclaration));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();
            codeDeclaration = Convert.ToInt32(commandSql.LastInsertedId);
            msc.Close();
        }
        public void AjouterDeclaration()
        {
            MySqlConnection msc = DataBaseAccess.GetConnexion;
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _insertSqll;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?codeDeclaration", codeDeclaration));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?commentaireDeclaration", commentaireDeclaration));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?dateDeclaration", dateDeclaration));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?urgence", urgence));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?traite", traite));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?codeProbleme", codeProbleme));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?numContainer", numContainer));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();
            msc.Close();
        }
        private void SaveDeclaration()
        {
            foreach (Declaration declarationCourante in _sesDeclarations)
            {
                declarationCourante.Save();
            }
        }

        #endregion

        public static List<Declaration> FetchAll(int traite)
        {
            List<Declaration> resultat = new List<Declaration>();
            MySqlConnection msc = DataBaseAccess.GetConnexion;
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _selectByTraiteSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?traite", traite));
            commandSql.Prepare();
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            while (jeuEnregistrements.Read())
            {
                Declaration declaration = new Declaration();
                declaration.codeDeclaration = Convert.ToInt16(jeuEnregistrements["codeDeclaration"].ToString());
                declaration.commentaireDeclaration = jeuEnregistrements["commentaireDeclaration"].ToString();
                declaration.dateDeclaration = Convert.ToDateTime(jeuEnregistrements["dateDeclaration"].ToString());
                declaration.urgence = Convert.ToInt16(jeuEnregistrements["urgence"].ToString());
                declaration.traite = Convert.ToInt16(jeuEnregistrements["traite"].ToString());
                declaration.codeProbleme = Convert.ToInt16(jeuEnregistrements["codeProbleme"].ToString());
                if (Convert.ToInt16(jeuEnregistrements["codeProbleme"].ToString()) == 2)
                {
                    declaration.libelleProbleme = "Corrosion";
                }
                if (Convert.ToInt16(jeuEnregistrements["codeProbleme"].ToString()) == 3)
                {
                    declaration.libelleProbleme = "Choc sur container";
                }
                if (Convert.ToInt16(jeuEnregistrements["codeProbleme"].ToString()) == 4)
                {
                    declaration.libelleProbleme = "Gong defectueux";
                }
                if (Convert.ToInt16(jeuEnregistrements["codeProbleme"].ToString()) == 5)
                {
                    declaration.libelleProbleme = "Problème de refrigeration";
                }
                declaration.numContainer = Convert.ToInt16(jeuEnregistrements["numContainer"].ToString());
                resultat.Add(declaration);
            }
            msc.Close();
            return resultat;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace TholdiPPE3
{
    class Travaux
    {
        private int codeTravaux;
        private string libelleTravaux;
        private decimal dureeImmobilisation;

        #region Requête SQL

        private static string _selectSql =
            "SELECT codeTravaux,libelleTravaux, dureeImmobilisation FROM TRAVAUX";

        private static string _selectBycodeTravauxSql =
            "SELECT codeTravaux,libelleTravaux, dureeImmobilisation FROM TRAVAUX WHERE codeTravaux = ?codeTravaux";

        #endregion

        #region méthode d'accès aux données

        public static Travaux Fetch(int codeTravaux)
        {
            Travaux t = null;
            MySqlConnection msc = DataBaseAccess.GetConnexion;
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _selectBycodeTravauxSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?codeTravaux", codeTravaux));
            commandSql.Prepare();
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            bool existEnregistrement = jeuEnregistrements.Read();
            if (existEnregistrement)
            {
                t = new Travaux();
                t.codeTravaux = Convert.ToInt32(jeuEnregistrements["codeTravaux"].ToString());
                t.libelleTravaux = jeuEnregistrements["libelleTravaux"].ToString();
                t.dureeImmobilisation = Convert.ToDecimal(jeuEnregistrements["dureeImmobilisation"].ToString());
            }
            msc.Close();
            return t;
        }

        public static List<Travaux> FetchAll()
        {
            List<Travaux> collectionTravaux = new List<Travaux>();
            MySqlConnection msc = DataBaseAccess.GetConnexion;
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _selectSql;
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            while (jeuEnregistrements.Read())
            {
                Travaux t = new Travaux();
                t.codeTravaux = Convert.ToInt32(jeuEnregistrements["codeTravaux"].ToString());
                t.libelleTravaux = jeuEnregistrements["libelleTravaux"].ToString();
                t.dureeImmobilisation = Convert.ToDecimal(jeuEnregistrements["dureeImmobilisation"].ToString());
                collectionTravaux.Add(t);
            }
            msc.Close();
            return collectionTravaux;
        }

        #endregion
    }
}

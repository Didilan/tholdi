﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace TholdiPPE3
{
    public partial class ChefSaisirUneInspection : Form
    {
        public ChefSaisirUneInspection()
        {
            InitializeComponent();
        }
       
        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }
        private void ChefSaisirUneInspection_Load (object sender , EventArgs e)
        {
          
            comboBox2.Items.Add("Prevue");
            comboBox2.Items.Add("En cours");
            comboBox2.Items.Add("Finie");
            comboBox1.Items.Add("Visite Annuelle");
            comboBox1.Items.Add("Reparation d'urgence");
            comboBox1.Items.Add("Recyclage");
            comboBox1.Items.Add("Autre");

            List<Containers> ListeCont = new List<Containers>();
            ListeCont = Containers.FetchAll();
            foreach(Containers C in ListeCont)
            {
                comboBox3.Items.Add(C.GetnumContainer);
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            
            int NumContainer = Convert.ToInt32(comboBox3.Text);
            DateTime dateInspection = Convert.ToDateTime(dateTimePicker1.Text);
            string avis = avisBox.Text;
            string commentaire = commentaireBox.Text;
            string motif = (comboBox1.Text).ToString();
            string etat = (comboBox2.Text).ToString();
            Inspection uneInspection = new Inspection(dateInspection, commentaire, avis, motif, etat, NumContainer);
            uneInspection.AjouterInspection();

            MessageBox.Show("Votre inspection a bien été validé.");
            Thread t = new Thread(() => Application.Run(new IhmChef()));
            t.Start();
            this.Close();
        }

        private void numInspectionBox_TextChanged(object sender, EventArgs e)
        {
          
        }


        private void numContainerBox_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void commentaireBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Thread t = new Thread(() => Application.Run(new IhmChef()));
            t.Start();
            this.Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace TholdiPPE3
{
    public partial class DockerConsulterUneInspection : Form
    {
        public DockerConsulterUneInspection()
        {
            InitializeComponent();
        }
        private void DockerConsulterInspection_Load(object sender, EventArgs e)
            {

            List<Containers> ListeCont = new List<Containers>();
            ListeCont = Containers.FetchAll();
            foreach (Containers C in ListeCont)
            {
                comboBox1.Items.Add(C.GetnumContainer);
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            int numContainer = Convert.ToInt16(comboBox1.Text);
            List<Inspection> collectionInspection = new List<Inspection>();
            collectionInspection = Inspection.FetchAll(numContainer);
            dataGridView1.DataSource = collectionInspection;
            dataGridView1.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
            dataGridView1.Columns[0].HeaderText = "Numéro Inspection";
            dataGridView1.Columns[1].HeaderText = "Etat";
            dataGridView1.Columns[2].HeaderText = "Commentaire";
            dataGridView1.Columns[3].HeaderText = "Avis";
            dataGridView1.Columns[4].HeaderText = "Motif";
            dataGridView1.Columns[5].HeaderText = "Numéro Container";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Thread t = new Thread(() => Application.Run(new IhmDocker()));
            t.Start();
            this.Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}

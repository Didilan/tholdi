﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;


namespace TholdiPPE3
{
    public partial class IhmChef : Form
    {
        public IhmChef()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Thread t = new Thread(() => Application.Run(new ChefSaisirUneInspection()));
            t.Start();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Thread t = new Thread(() => Application.Run(new ChefConsulterUneInspection()));
            t.Start();
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Thread t = new Thread(() => Application.Run(new ChefConsulterLesProblemes()));
            t.Start();
            this.Close();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Thread t = new Thread(() => Application.Run(new ChefPrevoirUneInspection()));
            t.Start();
            this.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Thread t = new Thread(() => Application.Run(new Form1()));
            t.Start();
            this.Close();
        }

        
        private void AideBoutton_Click(object sender, EventArgs e)
        {
            string filename = "C:\\Users\\dgou\\Desktop\\Noticeutilisateur.pdf";
            System.Diagnostics.Process.Start(filename);
        }
    }
}

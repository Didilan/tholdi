﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace TholdiPPE3
{
    public partial class ChefPrevoirUneInspection : Form
    {
        public ChefPrevoirUneInspection()
        {
            InitializeComponent();
        }
        private void ChefPrevoirInspection_Load(object sender , EventArgs e)
        {
            //List<String> uneListe = Inspection.FetchAllMotif();
            //foreach (String L in uneListe)
            //{
            //    comboBox2.Items.Add(L);
            //}
            comboBox2.Items.Add("Visite Annuelle");
            comboBox2.Items.Add("Reparation d'urgence");
            comboBox2.Items.Add("Recyclage");
            comboBox2.Items.Add("Autre");
            List<Containers> ListeCont = new List<Containers>();
            ListeCont = Containers.FetchAll();
            foreach (Containers C in ListeCont)
            {
                comboBox1.Items.Add(C.GetnumContainer);
            }

            
        }
        private void button2_Click(object sender, EventArgs e)
        {
            Thread t = new Thread(() => Application.Run(new IhmChef()));
            t.Start();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string motif = comboBox2.Text;
            int NumContainer = Convert.ToInt16(comboBox1.Text);
            DateTime dateInspection = Convert.ToDateTime(dateTimePicker1.Text);
            Inspection uneInspection = new Inspection(dateInspection, null, null, motif, "Prevue", NumContainer);
            uneInspection.AjouterInspection();

            MessageBox.Show("Votre inspection a bien été prévue.");
            Thread t = new Thread(() => Application.Run(new IhmChef()));
            t.Start();
            this.Close();

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace TholdiPPE3
{
    public partial class DockerSaisirUneDeclaration : Form
    {
        public DockerSaisirUneDeclaration()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void DockerSaisirUneDeclaration_Load(object sender, EventArgs e)
        {
            List < Probleme > listeProbleme = Probleme.FetchAll();
            foreach (Probleme P in listeProbleme)
            {
                comboBox3.Items.Add(P.GetlibelleProbleme);
            }          
            comboBox1.Items.Add("Oui");
            comboBox1.Items.Add("Non");
            List<Containers> ListeCont = new List<Containers>();
                ListeCont = Containers.FetchAll();
                 foreach (Containers C in ListeCont)
            {
                comboBox2.Items.Add(C.GetnumContainer);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Thread t = new Thread(() => Application.Run(new IhmDocker()));
            t.Start();
            this.Close();
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            int numContainer = Convert.ToInt32(comboBox2.Text);
            string commentaireDeclaration = textBoxCommentaire.Text;
            DateTime dateDeclaration = Convert.ToDateTime(dateTimePicker1.Text);
            int urgence = 0;
            int codeProbleme = Probleme.LaComparaison(comboBox3.Text);
            #region Urgence & Problème
            if (comboBox1.Text == "Oui")
            {
                urgence = 1;
            }
            
            #endregion

            Declaration uneDeclaration = new Declaration(commentaireDeclaration, dateDeclaration, urgence, 0, codeProbleme, numContainer);
            uneDeclaration.AjouterDeclaration();

            MessageBox.Show("Votre déclaration a bien été validé.");
            Thread t = new Thread(() => Application.Run(new IhmDocker()));
            t.Start();
            this.Close();
        }
    }
}

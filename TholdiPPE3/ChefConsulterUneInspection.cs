﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace TholdiPPE3
{
    public partial class ChefConsulterUneInspection : Form
    {
        public ChefConsulterUneInspection()
        {
            InitializeComponent();
        }
        private void inspectionPrevues_Load (object sender ,EventArgs e)
        {
            
            string etatF ="Finie";
            List<Inspection> collectionInspectionF = new List<Inspection>();
            collectionInspectionF = Inspection.FetchAll(etatF);
            inspectionAcheves.DataSource= collectionInspectionF;
            inspectionAcheves.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);

            #region Nom des columns
            inspectionAcheves.Columns[0].HeaderText = "Numéro Inspection";
            inspectionAcheves.Columns[1].HeaderText = "Etat";
            inspectionAcheves.Columns[2].HeaderText = "Commentaire";
            inspectionAcheves.Columns[3].HeaderText = "Avis";
            inspectionAcheves.Columns[4].HeaderText = "Motif";
            inspectionAcheves.Columns[5].HeaderText = "Numéro Container";
            #endregion 

            string etatP = "Prevue";
            List<Inspection> collectionInspectionP = new List<Inspection>();
            collectionInspectionP = Inspection.FetchAll(etatP);
            inspectionPrevues.DataSource = collectionInspectionP;
            inspectionPrevues.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);

            #region Nom des columns
            inspectionPrevues.Columns[0].HeaderText = "Numéro Inspection";
            inspectionPrevues.Columns[1].HeaderText = "Etat";
            inspectionPrevues.Columns[2].HeaderText = "Commentaire";
            inspectionPrevues.Columns[3].HeaderText = "Avis";
            inspectionPrevues.Columns[4].HeaderText = "Motif";
            inspectionPrevues.Columns[5].HeaderText = "Numéro Container";
            #endregion 

            string etatE = "En Cours";
            List<Inspection> collectionInspectionE = Inspection.FetchAll(etatE);
            inspectionCours.DataSource = collectionInspectionE;
            inspectionCours.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);

            #region Nom des columns
            inspectionCours.Columns[0].HeaderText = "Numéro Inspection";
            inspectionCours.Columns[1].HeaderText = "Etat";
            inspectionCours.Columns[2].HeaderText = "Commentaire";
            inspectionCours.Columns[3].HeaderText = "Avis";
            inspectionCours.Columns[4].HeaderText = "Motif";
            inspectionCours.Columns[5].HeaderText = "Numéro Container";
            #endregion 

        }

        private void button4_Click(object sender, EventArgs e)
        {
            Thread t = new Thread(() => Application.Run(new IhmChef()));
            t.Start();
            this.Close();
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
